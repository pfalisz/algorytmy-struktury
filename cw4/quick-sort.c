#include <stdio.h>
#include <stdlib.h>

#define N 1000

void stworzTablice(int tab[N]) {
		int i;
		time_t tt;
		srand(time(&tt));
		
		for(i = 0; i <N; i++) {
			tab[i] = rand() % N;
		}
}

void Quick(int *t, int a, int b) {
	int i,j,x;
	int tmp;
	
	x = t[a];
	i = a;
	j = b;
	
	do {		
		while(t[i] < x)
			i++;
		while(t[j] > x)
			j--;
			
		if(i <= j) {
			tmp = t[i];
			t[i] = t[j];
			t[j] = tmp;
			
			i++;
			j--;
		}
	} while(i <= j);
	

	if(a < j) Quick(t,a,j);
	if(b > i) Quick(t,i,b);

}

void QuickSort(int *t, int n) {
	Quick(t,0,n-1);
}

int main(void) {
	int tab[N] = {0,3,1,2,3,9,8};

	QuickSort(tab, N);

	return 0;
}