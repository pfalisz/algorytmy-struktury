#include <stdio.h>

#define N 200

void swap(int * a, int * b ){
	int tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}

void wypisz(int tab[], int r) {
	int i;
	for(i = 0; i < r; i++) printf("%d ", tab[i]);
}

unsigned short int Sprawdz(int tab[], const unsigned int r) {
	unsigned int i;
	for(i = 0; i < r-1; i++) {
		if(tab[i] > tab[i+1]){
			return 1;
		}
	} 
	
	return 0;
}

int Sort_Bubble(int tab[], int r) {
		int i,j;
		int licznik; 
		for (i = 0; i < r; i++) {
		 	for (j = 0; j < r-i-1; j++) {
		 			if(tab[j] > tab[j+1]) {
		 				swap(&tab[j], &tab[j+1]);
		 			}
					//printf("%d %d \n", j, j+1); 	
					licznik++;
		 	}
		} 

		
		return licznik;
		//printf("Licznik wykonal sie: %d \n", licznik);
}


int Sort_Insert(int tab[], int r) {
		int i, j, licznik;
		 
		for (i = 0; i < r; i++) {
			for (j = i; j > 0; j--) {
					licznik++;
					if(tab[j] < tab[j-1]) {
						swap(&tab[j], &tab[j-1]);
					}else {
						break;
					}
		
			}
		}

		return licznik;
}

int main(){
	int tab[N];
	int i;
	
	for(i = 0; i < N; i++) tab[i] = rand() % 100;
			 
//	printf("PRZED \n");		 
	//wypisz(tab, N);
 
	printf("Sortowanie babelkowe: %d", Sort_Bubble(tab, N));
	printf("\n");
	printf("Sortowanie przez wstawianie: %d", Sort_Insert(tab, N));
	
	//printf("PO \n");		 
	//wypisz(tab, N);
	 
	
	printf("\n\n");	
	
	
	if(!Sprawdz(tab, N)) {
		printf("POSORTOWANA!");
	} else {
		printf("NIEPOSORTOWANA!");
	};
	
	return 0;
}
