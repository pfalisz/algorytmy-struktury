#include <stdio.h>

#define N 5

void swap(int * a, int * b ){
	int tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}

unsigned short int Sprawdz(int tab[], const unsigned int r) {
	unsigned int i;
	for(i = 0; i < r-1; i++) {
		if(tab[i] > tab[i+1]){
			return 1;
		}
	} 
	
	return 0;
}

void Sortuj(int tab[], int r) {
		int i,j;
		int licznik; 
		for (i = 0; i < r; i++) {
		 	for (j = 0; j < r-i-1; j++) {
		 			if(tab[j] > tab[j+1]) {
		 				swap(&tab[j], &tab[j+1]);
		 			}
					//printf("%d %d \n", j, j+1); 	
					licznik++;
		 	}
		} 
		printf("\n");
		printf("\n");
		//printf("Licznik wykonal sie: %d \n", licznik);
}

int main(){
	int tab[N];
	int i;
	
	for(i = 0; i < N; i++) tab[i] = rand() % 100;
			 
	printf("PRZED \n");		 
	for(i = 0; i < N; i++) printf("%d ", tab[i]);
 
	Sortuj(tab, N);
	
	printf("PO \n");		 
	for(i = 0; i < N; i++) printf("%d ", tab[i]);
	
	
	printf("\n\n");	
	
	
	if(!Sprawdz(tab, N)) {
		printf("POSORTOWANA!");
	} else {
		printf("NIEPOSORTOWANA!");
	};
	
	return 0;
}
