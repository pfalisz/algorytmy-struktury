#include <stdio.h>

//uzytkownik podaje zakres x dla funkcji kwadratowej 
//wpsolczynniki(a,b,c), xo-x1, ilosc wartosci


void ReadFactor(double *v, char* c) {
	printf("%s: ", c);
	scanf("%lf", v);
}

double CountDelta(double a, double b, double c) {
	return b*b - 4*a*c;
}

double CountValues(double x1, double x2) {
	return  abs(x1) + abs(x2) + 1; //for now we just add 1 to calculate also 0
}

void CalculateValuesAndWriteToFile(int startX, int countValues, int a, int b, int c) { //TODO: put a,b,c in some kind of array, or struct 
	int y,x;
	FILE *f = fopen("wyniki.txt", "w");
	if (f == NULL)
	{
	    printf("Error opening file!\n");
	    exit(1);
	}
	
	fprintf(f, "L.p	|	x	|	y	\n");
	for(int i = 0; i < countValues; i++) {
		x = startX + i;
		y = a*(x*x) + b*x + c; 
		fprintf(f,"%d\t|\tx:%d\t|\ty:%d\n",i,x,y);  
	} 

}


double main() {
	
	double a,b,c;
	double x1, x2;
	double n;
	
	double delta; 
	printf("Podaj wspolczynniki a,b,c funkcji: \n");
	
	ReadFactor(&a, "a");
	ReadFactor(&b, "b");
	ReadFactor(&c, "c");
	
	printf("\nWzor funkcji to: %dx^2%dx%d\n\n",(int)a,(int)b,(int)c);
	
	printf("Podaj zakres, x1 i x2: \n");
	ReadFactor(&x1, "x1");
	ReadFactor(&x2, "x2");
	
	
	printf("Podaj ilosc wartosci: \n");
	ReadFactor(&n, "n");
	printf("Delta: %.2f \n\n", CountDelta(a,b,c));


	CalculateValuesAndWriteToFile(x1, CountValues(x1,x2), a,b,c);
	printf("Pomyslnie zapisano do pliku!");

	return 0;
}