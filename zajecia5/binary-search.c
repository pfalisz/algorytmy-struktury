#include <stdio.h>
#include <stdlib.h>
#define N 20000
 
void Wypisz(int * tab, int n);
int WyszukajZwykle(int * tab, int n, int szukana);
int WyszukajBinarnie(int * tab, int n, int szukana);
void Sortuj(int * tab, int n);
 
int main() {
        int * tab = (int*)malloc(sizeof(int) * N), i;
        for(i = 0 ; i < N; i++) tab[i] = rand() % 1000;
        
        Sortuj(tab, N);
        
        for( i = 0 ; i < 500000; i++) {
                WyszukajBinarnie(tab, N, i);
        }
        
        /*
        for( i = 0 ; i < 500000; i++) {
                WyszukajZwykle(tab, N, i);
        }*/
        
        free(tab);
        return 0;
}
 
void Wypisz(int * tab, int n) {
        int i;
        for(i =0 ; i < n; i++) {
                printf("%d ", tab[i]);
        }
        printf("\n");
}
 
int WyszukajZwykle(int * tab, int n, int szukana) {
        //return -1 gdy liczby nie ma
        //0 .. +inf gdy liczba jest i zwraca indeks tablicy
        int i;
        for(i = 0 ; i < n; i++) {
                if(tab[i] == szukana) return i;
        }
        return -1;
}
 
void Sortuj(int * tab, int n) {
        int i,j;
        for( i = 0 ; i < n; i++) {
                for(j = 0 ; j < n - 1 - i; j++) {
                        if(tab[j] > tab[j+1]) {
                                int tmp = tab[j];
                                tab[j] = tab[j+1];
                                tab[j+1] = tmp;
                        }
                }
        }
}
 
int BinarnaHelp(int * tab, int Od, int Do, int szukana) {
        int IdxSr = (Do+Od)/2;
        if(tab[IdxSr] == szukana) return IdxSr;
        
        if( (Do-Od) > 1) {
                if(tab[IdxSr] < szukana) {
                        BinarnaHelp(tab, IdxSr, Do,szukana);
                } else {
                        BinarnaHelp(tab, Od, IdxSr,szukana);
                }
        } else {
                return -1;
        }
}
 
int WyszukajBinarnie(int * tab, int n, int szukana) {
        return BinarnaHelp(tab, 0, n-1, szukana);
}