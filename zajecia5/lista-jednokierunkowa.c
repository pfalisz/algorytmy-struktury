#include <stdio.h>
#include <stdlib.h>
 
struct Wezel{
        int dane;
        struct Wezel * nastepny;
};
 
void Dodaj(struct Wezel ** poczatek, int liczbaDoDodania, int indeks);
int Pobierz(struct Wezel * poczatek, int indeks);
int Usun(struct Wezel * poczatek, int indeks);
void Wypisz(struct Wezel * poczatek);
 
int main() {
        struct Wezel * poczatek = 0;
        
        Dodaj(&poczatek, 15, 0);
        Dodaj(&poczatek, 16, 1);
        Dodaj(&poczatek, 17, 0);
        Dodaj(&poczatek, 15, 2);
        Dodaj(&poczatek, 16, 0);
        Dodaj(&poczatek, 17, 3);
        
        Wypisz(poczatek);
        
        printf("\nIndeks4 : %d\n", Pobierz(poczatek, 5));
                
        return 0;
}
 
void Dodaj(struct Wezel ** poczatek, int liczbaDoDodania, int indeks) {
        struct Wezel * nowy = (struct Wezel*)malloc(sizeof(struct Wezel));
        nowy->dane = liczbaDoDodania;
        
        if(*poczatek) {
                //Lista istnieje
                if(indeks == 0) { //dodawanie na pocz�tek listy
                        nowy->nastepny = *poczatek;
                        *poczatek = nowy;
                } else {
                        struct Wezel * tmp = *poczatek;
                        int i;
                        for( i = 0 ; i < indeks-1; i++) {
                                tmp = tmp->nastepny;
                        }
                        //procedura dodawania
                        nowy->nastepny = tmp->nastepny;
                        tmp->nastepny = nowy;
                }
        } else {
                //Lista nie istnieje    
                nowy->nastepny = 0;
                
                *poczatek = nowy;
        }
}
 
int Pobierz(struct Wezel * poczatek, int indeks) {
        struct Wezel * tmp = poczatek;
        int i;
        for(i = 0 ; i < indeks; i++) {
                tmp = tmp->nastepny;    
        }
        return tmp->dane;
}
 
void Wypisz(struct Wezel * poczatek) {
        struct Wezel * tmp = poczatek;
        int i;
        while(tmp) {
                printf("%d ", tmp->dane);
                tmp = tmp->nastepny;    
        }
}

int Usun(struct wezel **p, int pozycja){
        struct wezel *tmp = *p;
        
        if(pozycja){
                int i;
                for(i=0; i<pozycja-1; ++i){
                        tmp = tmp->nastepny;
                }
                struct wezel *doUsuniecia = tmp->nastepny;
                int zwrotka = tmp->nastepny/*doUsuniecia*/->Dane;
                
                tmp->nastepny = doUsuniecia->nastepny;
                free(doUsuniecia);
                
                return zwrotka;
        }
        else{
                int zwrotka = tmp->Dane;
                *p = tmp->nastepny;
                
                free(tmp);
                return zwrotka;
        }
}