#include <stdio.h>
#include <stdlib.h>

int sorted = 0;
int GenerujTablice(int n) {
	
	int * tab = (int*)malloc(n * sizeof(int)), i;
	
	time_t tt;
	srand(time(&tt));
	
	for(i=0; i < n; i++) {
		tab[i] = rand();
	}
	
	return tab;	
}

void WypiszTablice(int tab[], int r) {
	int i;
	sorted == 1 ? printf("\nPosortowana tablica: \n\n") : printf("\nNieposortowana tablica: \n\n");
	
	for(i=0; i < r; i++) {
		printf("%d\t", tab[i]);
	}
	
	
	
}

void SortujTablice(int tab[], int k) {
	int i;
	int tmp;
	sorted = 0;	
	
	
	while(sorted == 0) {
	
		int change = 0;
		for(i=0; i < k; i++) {
			
			if(tab[i] > tab[i+1]) {
				tmp = tab[i];
				tab[i] =  tab[i+1];
				tab[i+1] = tmp;
				change++;
			}
		
			if(k-1 == i && change == 0) {
				sorted = 1;
			} 
		}	
	}
		
}

int main() {
	int k = 10;
	int i, * tab;
	
	tab = GenerujTablice(k);
	WypiszTablice(tab, k);
	SortujTablice(tab, k);
	printf("\n");
	printf("\n");
	WypiszTablice(tab, k);

	
	free(tab);
	
	return 0;
}