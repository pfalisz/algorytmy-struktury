#include <stdio.h>
#include <stdlib.h>

struct Student  {
	int Wiek;
	double Ocena;
};

void Ustaw(struct Student *s, int r) {
	int i;
	int Od, Do;	
	time_t tt;
	srand(time(&tt));
	

	for(i=0; i <r; i++){

		Od = 19;
		Do = 30;
		s[i].Wiek = rand() % (Do-Od) + Od;
		
		Od = 2;
		Do = 5;
		s[i].Ocena = (double)rand() / RAND_MAX * (Do-Od) + Od;
		
		}
}

double Zao(double x) {
	double tmp = x - (int)x;
	return tmp < 0.5 ? (int)x : (int)x + 0.5;
}

void Wypisz(struct Student *s, int r) {
	int i;

	for(i=0; i <r; i++){
		printf("ID: %d, Wiek: %d, Ocena: %.1f \n", i, s[i].Wiek, Zao(s[i].Ocena));
	}

}



int main(){
	const unsigned int N = 15;
	
	struct Student s[N];
	
	Ustaw(s,N);
	Wypisz(s,N);
	
	

	

	return 0;
}